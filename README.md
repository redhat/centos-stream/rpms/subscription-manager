subscription-manager
====================

The Subscription Manager package provides programs and libraries
to allow users to manage subscriptions and yum/dnf repositories
from the [Candlepin](http://candlepinproject.org/) server.

- Project homepage: http://candlepinproject.org/
- Upstream repository: https://github.com/candlepin/subscription-Manager

## Contribution Guidelines

If you want to do any changes to `subscription-manager.spec`, then please create pull request with same/similar change to upstream repository, because upstream repository manages its own .spec file. If the change would not be contributed to upstream repository, then there is risk that the change provided here could be reverted later by upstream.
