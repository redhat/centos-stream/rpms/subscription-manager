#!/bin/bash


while getopts ":c" opt; do
  case $opt in
    c) IN_CONTAINER=true;;
    *) IN_CONTAINER=false;;
  esac
done 

# takes in a command and makes sure that it returns success
# and that nothing is sent to stderr
function smoke {
  echo -n "Smoke test: '$@': "
  ERROR=$("$@" 2>&1> /dev/null)
  RETVAL=$?
  
  if [ -z "$ERROR" ] && [[ $RETVAL == 0 ]]; then
    echo "PASS"
  else
    echo "FAIL"
    echo "RETVAL: $RETVAL"
    echo "STDERR: $ERROR"
    exit 1
  fi
}

function smoke_container {
  echo -n "Smoke Container test: '$@': "
  ERROR=$("$@" 2>&1> /dev/null)
  RETVAL=$?
  
  if [[ ! -z "$ERROR" ]] && [[ $RETVAL == 78 ]]; then
    echo "PASS"
  else
    echo "FAIL"
    echo "RETVAL: $RETVAL"
    echo "STDERR: $ERROR"
    exit 1
  fi
}

SMOKE_CMDS="subscription-manager --help
subscription-manager config
subscription-manager facts
subscription-manager repos
subscription-manager version"

if [[ "$IN_CONTAINER" == "true" ]]; then
  TEST_CMD="smoke_container"
  SMOKE_CMDS="subscription-manager config
subscription-manager facts
subscription-manager repos
subscription-manager version"
else
  TEST_CMD="smoke"
fi

while read -r CMD; do
  $TEST_CMD $CMD
done <<<"$SMOKE_CMDS"
